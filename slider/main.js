(function() {
    var slideIndex = 0;
    var timerId;
    var imagesList = [
        'imgs/image1.jpg',
        'imgs/image2.jpg',
        'imgs/image3.jpg',
        'imgs/image4.jpg',
        'imgs/image5.jpg',
        'imgs/image6.jpg'
    ];
    var len=imagesList.length;
    //var slider;
    var showPict;
    // Execute only once on page is loaded
    function onInit() {
        //slider = document.getElementById('slider');
        document.getElementById("slider").innerHTML = '<div id="showPict"></div><button id="prev" class="btn">&#10094;</button><button id="next" class="btn">&#10095;</button>';
        var img = document.createElement("IMG");                 
        img.setAttribute("src", imagesList[0]);
        img.setAttribute("class","img_o res");         
        showPict = document.getElementById("showPict")
        showPict.appendChild(img);
        document.getElementById("next").addEventListener("click", nextSlide);
        document.getElementById("prev").addEventListener("click", previousSlide);
        timerId = setTimeout(nextSlide_timer, 3000);
    start_time();
        
    }

    // Execute on next button click
    function nextSlide() {
                if (slideIndex < len-1){slideIndex+=1;} else 
                {slideIndex=0};   
                var img = document.createElement("IMG");                 
                img.setAttribute("src", imagesList[slideIndex]);
                img.setAttribute("class","img_o res");
                var oldImg = document.getElementsByClassName("img_o res")[0];       
                showPict.replaceChild(img, oldImg);
                console.log(slideIndex);
                drop_time();
                start_time();
                 }


    function nextSlide_timer() {
                if (slideIndex < len-1){
                    slideIndex+=1;
                } else {
                    slideIndex=0
                };   
                var img = document.createElement("IMG");                 
                img.setAttribute("src", imagesList[slideIndex]);
                img.setAttribute("class","img_o res");
                var oldImg = document.getElementsByClassName("img_o res")[0];       
                showPict.replaceChild(img, oldImg);
                drop_time();
                start_time();
                } 
    // Execute on previous button click
    function previousSlide() {
            
                if (slideIndex <= 0){slideIndex=len-1} else {slideIndex-=1;}
                var img = document.createElement("IMG");                 
                img.setAttribute("src", imagesList[slideIndex]);
                img.setAttribute("class","img_o res");
                var oldImg = document.getElementsByClassName("img_o res")[0];       
                showPict.replaceChild(img, oldImg);
                console.log(slideIndex);
                drop_time();
                start_time();
    }
    
    function drop_time() {
    clearInterval(timerId);
    }
    function start_time() {
    timerId = setTimeout(nextSlide_timer, 5000);
    }
    
   

    document.addEventListener('DOMContentLoaded', onInit);
     
})();
